//
// Created by ignac on 25.04.2020.
//
#include <iostream>
#include "Mainsweeperboard.h"

using namespace std;
//----------------------------------------------------------------------------------------------------------------------
Mainsweeperboard::Mainsweeperboard(int a) {
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
           tab[i][j]=a;
        }
    }
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::debug_display() {
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            show(tab[i][j]);
        }
        cout << endl;
    }
}

//----------------------------------------------------------------------------------------------------------------------
    void Mainsweeperboard::show(int b){
        if(b==0) cout << " [   ] ";
        if(b==10) cout << " [ B ] ";
        if(b==1) cout << " [ 1 ] ";
        if(b==2) cout << " [ 2 ] ";
        if(b==3) cout << " [ 3 ] ";
        if(b==4) cout << " [ 4 ] ";
        if(b==5) cout << " [ 5 ] ";
        if(b==6) cout << " [ 6 ] ";
        if(b==7) cout << " [ 7 ] ";
        if(b==8) cout << " [ 8 ] ";
    }
//----------------------------------------------------------------------------------------------------------------------
    void Mainsweeperboard::level(){
        poczatek:
        cout << "Wybierz poziom trudnosci: " << endl;
        cout << "Wpisz 1 - latwy" << endl;
        cout << "Wpisz 2 - sredni" << endl;
        cout << "Wpisz 3 - trudny" << endl;
        cin >> numberlevel;
        if(numberlevel != 1 && numberlevel != 2 && numberlevel != 3)
        {
            cout << "Zle wybrana liczba, sprobuj jeszcze raz: ";
            goto poczatek;
        }
    }
//----------------------------------------------------------------------------------------------------------------------
    void Mainsweeperboard::randomizing() {
        if(numberlevel==1)
        {
            start1:
            srand(time(NULL));
            for(int i=0; i<10; i++)
            {
                easyB[i]=rand()%100+1;
            }
            for(int i=0; i<10; i++)
            {
                for(int j=i+1; j<10; j++)
                {
                    if(easyB[i]==easyB[j])      goto start1;
                }
            }
            /*for(int i=0; i<10; i++)
            {
                cout << easyB[i] << endl;
            }*/
        }
        else if(numberlevel==2)
        {
            start2:
            srand(time(NULL));
            for(int i=0; i<20; i++)
            {
                normalB[i]=rand()%100+1;
            }
            for(int i=0; i<20; i++)
            {
                for(int j=i+1; j<20; j++)
                {
                    if(normalB[i]==normalB[j])      goto start2;
                }
            }
            /*for(int i=0; i<20; i++)
            {
                cout << normalB[i] << endl;
            }*/
        }
        if(numberlevel==3)
        {
            start3:
            srand(time(NULL));
            for(int i=0; i<30; i++)
            {
                hardB[i]=rand()%100+1;
            }
            for(int i=0; i<30; i++)
            {
                for(int j=i+1; j<30; j++)
                {
                    if(hardB[i]==hardB[j])      goto start3;
                }
            }
            /*for(int i=0; i<30; i++)
            {
                cout << hardB[i] << endl;
            }*/
        }
    }
//----------------------------------------------------------------------------------------------------------------------
    void Mainsweeperboard::boombing() {

        if(numberlevel==1)
        {
            for(int i=0; i<10; i++)
            {
                tab[easy_first[i]][easy_second[i]]=10;
            }
        }
        if(numberlevel==2)
        {
            for(int i=0; i<20; i++)
            {
                tab[normal_first[i]][normal_second[i]]=10;
            }
        }
        if(numberlevel==3)
        {
            for(int i=0; i<30; i++)
            {
                tab[hard_first[i]][hard_second[i]]=10;
            }
        }
    }
//----------------------------------------------------------------------------------------------------------------------
    void Mainsweeperboard::dispensation() {

        if(numberlevel==1)
        {
            for(int i=0; i<10; i++)
            {
                easy_second[i]=easyB[i]%10;
                easy_first[i]=(easyB[i]-easy_second[i])/10;
            }
            //cout << endl << "---------------------------------------------------------------------" << endl;
            /*for(int i=0; i<10; i++)
            {
                cout << easy_first[i] << " , " << easy_second[i] << endl;
            }*/
        }
        if(numberlevel==2)
        {
            for(int i=0; i<20; i++)
            {
                normal_second[i]=normalB[i]%10;
                normal_first[i]=(normalB[i]-normal_second[i])/10;
            }
            //cout << endl << "---------------------------------------------------------------------" << endl;
            /*for(int i=0; i<20; i++)
            {
                cout << normal_first[i] << " , " << normal_second[i] << endl;
            }*/
        }
        if(numberlevel==3)
        {
            for(int i=0; i<30; i++)
            {
                hard_second[i]=hardB[i]%10;
                hard_first[i]=(hardB[i]-hard_second[i])/10;
            }
            //cout << endl << "---------------------------------------------------------------------" << endl;
            /*for(int i=0; i<30; i++)
            {
                cout << hard_first[i] << " , " << hard_second[i] << endl;
            }*/
        }
    }
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::gameboard() {
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            gameshow(gametab[i][j]);
        }
        cout << endl;
    }
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::gameshow(int a) {
    if(a==100)      cout << " [   ] ";
    if(a==0)        cout << " [ 0 ] ";
    if(a==1)        cout << " [ 1 ] ";
    if(a==2)        cout << " [ 2 ] ";
    if(a==3)        cout << " [ 3 ] ";
    if(a==4)        cout << " [ 4 ] ";
    if(a==5)        cout << " [ 5 ] ";
    if(a==6)        cout << " [ 6 ] ";
    if(a==7)        cout << " [ 7 ] ";
    if(a==8)        cout << " [ 8 ] ";
    if(a==10)       cout << " [BBB] ";
    if(a==11)       cout << " [FFF] ";
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::loadgameboard() {
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            gametab[i][j]=100;
        }
    }


}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::loadnumbers() {
    for(int i=1; i<9; i++)
    {
        for(int j=1; j<9; j++)
        {
            if(tab[i][j]==0)
            {
                if(tab[i-1][j-1]==10)   tab[i][j]+=1;
                if(tab[i-1][j]==10)     tab[i][j]+=1;
                if(tab[i-1][j+1]==10)   tab[i][j]+=1;
                if(tab[i][j-1]==10)     tab[i][j]+=1;
                if(tab[i][j+1]==10)     tab[i][j]+=1;
                if(tab[i+1][j-1]==10)   tab[i][j]+=1;
                if(tab[i+1][j]==10)     tab[i][j]+=1;
                if(tab[i+1][j+1]==10)   tab[i][j]+=1;
            }
        }
    }
    for(int i=1; i<9; i++)
    {
        if(tab[0][i]==0)
        {
            if(tab[0][i-1]==10)     tab[0][i]+=1;
            if(tab[0][i+1]==10)     tab[0][i]+=1;
            if(tab[1][i-1]==10)     tab[0][i]+=1;
            if(tab[1][i]==10)       tab[0][i]+=1;
            if(tab[1][i+1]==10)     tab[0][i]+=1;
        }
        if(tab[9][i]==0)
        {
            if(tab[9][i-1]==10)     tab[9][i]+=1;
            if(tab[9][i+1]==10)     tab[9][i]+=1;
            if(tab[8][i-1]==10)     tab[9][i]+=1;
            if(tab[8][i]==10)       tab[9][i]+=1;
            if(tab[8][i+1]==10)     tab[9][i]+=1;
        }
        if(tab[i][0]==0)
        {
            if(tab[i-1][0]==10)     tab[i][0]+=1;
            if(tab[i+1][0]==10)     tab[i][0]+=1;
            if(tab[i-1][1]==10)     tab[i][0]+=1;
            if(tab[i][1]==10)       tab[i][0]+=1;
            if(tab[i+1][1]==10)     tab[i][0]+=1;
        }
        if(tab[i][9]==0)
        {
            if(tab[i-1][9]==10)     tab[i][9]+=1;
            if(tab[i+1][9]==10)     tab[i][9]+=1;
            if(tab[i-1][8]==10)     tab[i][9]+=1;
            if(tab[i][8]==10)       tab[i][9]+=1;
            if(tab[i+1][8]==10)     tab[i][9]+=1;
        }
    }
    if(tab[0][0]==0)
    {
        if(tab[0][1]==10)      tab[0][0]+=1;
        if(tab[1][1]==10)      tab[0][0]+=1;
        if(tab[1][0]==10)      tab[0][0]+=1;
    }
    if(tab[0][9]==0)
    {
        if(tab[0][8]==10)      tab[0][9]+=1;
        if(tab[1][8]==10)      tab[0][9]+=1;
        if(tab[1][9]==10)      tab[0][9]+=1;
    }
    if(tab[9][0]==0)
    {
        if(tab[8][0]==10)      tab[9][0]+=1;
        if(tab[8][1]==10)      tab[9][0]+=1;
        if(tab[9][1]==10)      tab[9][0]+=1;
    }
    if(tab[9][9]==0)
    {
        if(tab[8][8]==10)      tab[9][9]+=1;
        if(tab[8][9]==10)      tab[9][9]+=1;
        if(tab[9][8]==10)      tab[9][9]+=1;
    }
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::play() {
    for(;;)
    {
        cout << "Jezeli chcesz odkryc pole wpisz 1: " << endl;
        cout << "Jezeli chcesz oflagowac pole wpisz 2: " << endl;
        cin >> play_mode;
        if(play_mode==1 || play_mode==2) break;
    }
    if(play_mode==1)
    {
        cout << "wpisz nuemr rzedu: ";
        cin >> play_a;
        //cout << endl;
        cout << "wpisz numer kolumny: ";
        cin >> play_b;
       // cout << endl;
        loadplacegame(play_a, play_b);
        x++;
    }
    else if(play_mode==2)
    {
        cout << "wpisz nuemr rzedu: ";
        cin >> play_a;
        //cout << endl;
        cout << "wpisz numer kolumny: ";
        cin >> play_b;
        //cout << endl;
        loadflaggame(play_a, play_b);
    }
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::loadplacegame(int a, int b) {
    for(int i=0; i<number_of_flags; i++)
    {
        if(a==flag_a[i] && b==flag_b[i])
        {
            there_is_a_flag();
            goto koniec;
        }
    }
    gametab[a][b]=tab[a][b];
    if(gametab[a][b]==10)
    {
        youloose();
        goto koniec;
    }

koniec:;
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::loadflaggame(int a, int b) {
    gametab[a][b]=11;
    flag_a[number_of_flags]=a;
    flag_b[number_of_flags]=b;
    number_of_flags++;
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::youloose() {
system("cls");
cout << endl << "PRZEGRANA" << endl;
}
//----------------------------------------------------------------------------------------------------------------------
void Mainsweeperboard::there_is_a_flag() {
    cout << "To pole jest oflagowane!" << endl;
}
//----------------------------------------------------------------------------------------------------------------------

