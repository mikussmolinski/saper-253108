//
// Created by ignac on 25.04.2020.
//

#ifndef SAPER_CLION_MAINSWEEPERBOARD_H
#define SAPER_CLION_MAINSWEEPERBOARD_H


#include <iostream>
#include <cstdlib>
#include <ctime>

class Mainsweeperboard {
    int tab[10][10];
    int easyB[10];
    int normalB[20];
    int hardB[30];
    int easy_first[10], easy_second[10];
    int normal_first[20], normal_second[20];
    int hard_first[30], hard_second[30];
    int play_a, play_b, play_mode;
    int number_of_flags{0};
    int flag_a[100]{999}, flag_b[100]{999};


public:
    int x{0};
    int numberlevel;
    int gametab[10][10];
    Mainsweeperboard(int a);
    void debug_display();
    static void show(int b);
    void level();
    void randomizing();
    void boombing();
    void dispensation();
    void gameboard();
    void gameshow(int a);
    void loadgameboard();
    void loadnumbers();
    void play();
    void loadplacegame(int a, int b);
    void loadflaggame(int a, int b);
    void youloose();
    void there_is_a_flag();
};


#endif //SAPER_CLION_MAINSWEEPERBOARD_H
